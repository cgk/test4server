package com.shy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Test4serverApplication {

	public static void main(String[] args) {
		SpringApplication.run(Test4serverApplication.class, args);
	}
}
