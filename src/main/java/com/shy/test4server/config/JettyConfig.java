package com.shy.test4server.config;

import org.springframework.context.annotation.Configuration;

/**
 * @ClassName: JettyConfig
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author chengcai.shang@cmgplex.com
 * @date 2018年12月7日 上午9:53:46
 * 
 */
@Configuration
public class JettyConfig {
	// @Value("${jetty.thread.max}")
	// private Integer max;
	// @Value("${jetty.thread.min}")
	// private Integer min;
	//
	// @Bean
	// public JettyEmbeddedServletContainerFactory jettyEmbeddedServletContainerFactory(
	// JettyServerCustomizer jettyServerCustomizer) {
	// JettyEmbeddedServletContainerFactory factory = new JettyEmbeddedServletContainerFactory();
	// factory.addServerCustomizers(jettyServerCustomizer);
	// return factory;
	// }
	//
	// @Bean
	// public JettyServerCustomizer jettyServerCustomizer() {
	// return server -> {
	// this.threadPool(server);
	// };
	// }
	//
	// private void threadPool(Server server) {
	// // Tweak the connection config used by Jetty to handle incoming HTTP
	// // connections
	// final QueuedThreadPool threadPool = server.getBean(QueuedThreadPool.class);
	// // 默认最大线程连接数200
	// threadPool.setMaxThreads(this.max);
	// // 默认最小线程连接数8
	// threadPool.setMinThreads(this.min);
	// // 默认线程最大空闲时间60000ms
	// threadPool.setIdleTimeout(60000);
	// }
}
