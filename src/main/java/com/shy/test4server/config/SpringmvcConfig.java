package com.shy.test4server.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @ClassName: SpringmvcConfig
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2018年12月7日 上午9:59:06
 * 
 */
@Configuration
public class SpringmvcConfig extends WebMvcConfigurerAdapter {
	@Value("${springmvc.thread.core}")
	private Integer core;
	@Value("${springmvc.thread.max}")
	private Integer max;
	@Value("${springmvc.thread.queue}")
	private Integer queue;

	@Override
	public void configureAsyncSupport(AsyncSupportConfigurer asyncSupportConfigurer) {
		ThreadPoolTaskExecutor threadPool = new ThreadPoolTaskExecutor();
		threadPool.setCorePoolSize(this.core);
		threadPool.setMaxPoolSize(this.max);
		threadPool.setQueueCapacity(this.queue);
		threadPool.initialize();
		asyncSupportConfigurer.setTaskExecutor(threadPool);
	}
}
