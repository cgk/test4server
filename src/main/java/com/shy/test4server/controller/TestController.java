package com.shy.test4server.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.WebAsyncTask;

/**
 * @ClassName: TestController
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author chengcai.shang@cmgplex.com
 * @date 2018年12月7日 上午9:36:25
 * 
 */
@RestController
@RequestMapping("/test")
public class TestController {
	/**
	 * 未使用HTTP异步的接口
	 * 
	 * @Title: testCeilingNoAsync
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @date 2018年12月7日 上午9:40:57
	 */
	@GetMapping("/testCeilingNoAsync")
	public String testCeilingNoAsync() {
		return "";
	}

	/**
	 * 使用HTTP异步的接口
	 * 
	 * @Title: testCeilingNoAsync
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @date 2018年12月7日 上午9:40:57
	 */
	@GetMapping("/testCeilingWithAsync")
	public WebAsyncTask<String> testCeilingWithAsync() {
		return new WebAsyncTask(() -> {
			return "";
		});
	}
}
